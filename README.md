
# Pascal Debug Messages

Using Pascal Debug Messages, you can add points in code to log e.g. method entry and exit points, error messages, or variable content.

Output can be a console window, the Windows debug console, the Windows event log, simple log files or others. With a simple hook, debug output can be received by the software using it.

```
uses
   SysUtils,
   DebugLog;

// ...

procedure Bla(Something: string);
begin
   DebugLogger.LogEnterMethod('Bla', [Something]);
   try
      // do bla
   finally
      DebugLogger.LogLeaveMethod('Bla');
   end;
```