﻿{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Forwards debug messages to the console.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2010-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2018-04-10  pk   5m  Improved non-console testing to only be called on libraries
// *****************************************************************************
   )
}

unit DebugLog.Destinations.Console;

{$IFDEF FPC}
{$MODE Delphi}
{$ENDIF FPC}

interface

{
  This handler gets automatically registered as a destination for debug lines.
  It outputs debug information to the console.

  @param(ALine is the text it should output.)
}
procedure SendDebugLogLineToConsole(const ALine: WideString);

implementation

uses
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   DebugLog.Consts;

procedure SendDebugLogLineToConsole(const ALine: WideString);
begin
   if not (doConsole in DebugOutputs) then begin
      Exit;
   end;
   {$IFDEF MSWindows}
   if not IsConsole then begin
      IsConsole := AllocConsole;
   end;
   if IsConsole then begin
      Writeln(ALine);
   end else if IsLibrary then begin
      try
         Writeln(ALine);
      except
         // a DLL might not know it's host process is a console process
      end;
   end;
   {$ENDIF MSWindows}
end;

initialization

   RegisterDebugLogLineHandler(SendDebugLogLineToConsole);

end.
