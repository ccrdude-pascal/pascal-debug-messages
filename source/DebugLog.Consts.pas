﻿{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Constants used in handling debug messages.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2010-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-08-29  pk  10m  Fixed procedure test in IsRegisteredDebugLogLineHandler
// 2016-06-30  jh   --  PepiMK.Pipes.Classes for TDebugMessage.PostToPipe
// *****************************************************************************
   )
}

{$IFDEF FPC}
{$mode Delphi}{$H+}
// This needs to be Delphi for IsRegisteredDebugLogLineHandler!
{$modeswitch advancedrecords}
{$codepage utf8}
{$ELSE FPC}
{$DEFINE Windows}
{$ENDIF FPC}

unit DebugLog.Consts;

interface

uses
   SysUtils,
   Classes,
   Generics.Collections;

type
   EDebugLoggerException = class(Exception)

   end;

   TDebugLevel = (dlvDebug, // simply everything
      dlvVerbose,           // more details than usual
      dlvMessage,           // important information only
      dlvWarning,           // warnings and worse
      dlvError,             // errors and worse
      dlvFatal,             // exceptions only
      dlvControl,
      dlvPopup,             // shows important debug information in a popup
      dlvDefault            // use the default value
      );
   TDebugDirection = (dEnter, dLeave, dError, dMessage, dWarning);
   TDebugType = (dtNone, dtProcess, dtThread, dtMethod, dtUnitConstructor, dtUnitDestructor, dtVariable);
   TDebugOutput = (doDebugConsole, doConsole, doLogFile, doEventLog);
   TDebugOutputs = set of TDebugOutput;

   TDebugLogDataHandler = procedure(ADirection: TDebugDirection; AType: TDebugType; AObject: TObject; AText: WideString; const AArgs: array of const);
   TDebugLogLineHandler = procedure(const ALine: WideString);
   TDebugLogExceptionHandler = procedure(AException: Exception; AMessage: WideString);
   TDebugLogErrorHandler = procedure(AErrorMessage: WideString);

var
   DebugOutputs: TDebugOutputs;

const
   DebugPipeName: string = 'SNLDebugPipe';
   SDebugDirection: array [TDebugDirection] of char = ('>', '<', '!', 'i', '!');
   SDebugType: array [TDebugType] of string = ('', 'process', 'thread', 'method', 'unit initialization', 'unit finalization', 'variable');

type
   {
     TDebugMessageData is record representing debug messages.
   }
   TDebugMessageData = record
      ThreadID: cardinal;
      ProcessID: cardinal;
      Level: TDebugLevel;
      Direction: TDebugDirection;
      MessageType: TDebugType;
      MessageText: WideString;
      AffectedObject: TObject;
      UnitName: WideString;
      ClassName: WideString;
      Timestamp: TDateTime;
      {
        Assigns data from a different debug data object to this one.

        @param(ASource is the other object.)
      }
      procedure Assign(ASource: TDebugMessageData);
      {
        Returns unit name and class name as a prefix with dots as separators.
      }
      function ObjectPrefix: string;
      {
        Returns a text line with all debug data information.

        @param(ALastMessageTimestamp defines the last debug line, so that the difference can be calculated.)
        @returns(Text line.)
      }
      function ToLine(ALastMessageTimestamp: TDateTime): WideString;
   end;

   PDebugMessageData = ^TDebugMessageData;

   {
     TDebugMessage is a class representing debug messages.
   }
   TDebugMessage = class
   private
      FData: TDebugMessageData;
   public
      //class function PostToPipe(AMessageText: WideString; AObject: TObject = nil; AType: TDebugType = dtNone; ADirection: TDebugDirection = dMessage; ALevel: TDebugLevel = dlvDefault): boolean;
      constructor Create(AMessageText: WideString; AObject: TObject = nil; AType: TDebugType = dtNone; ADirection: TDebugDirection = dMessage; ALevel: TDebugLevel = dlvDefault); overload;
      constructor Create(AStream: TStream); overload;
      property ProcessID: cardinal read FData.ProcessID;
      property ThreadID: cardinal read FData.ThreadID;
      property Timestamp: TDateTime read FData.Timestamp;
      property Level: TDebugLevel read FData.Level;
      property Direction: TDebugDirection read FData.Direction;
      property MessageType: TDebugType read FData.MessageType;
      property AffectedObject: TObject read FData.AffectedObject;
      property MessageText: WideString read FData.MessageText;
      property ObjectUnitName: WideString read FData.UnitName;
      property ObjectClassName: WideString read FData.ClassName;
      property Data: TDebugMessageData read FData;
      procedure ToStringList(AList: TStrings);
      procedure ToStream(AStream: TStream);
   end;

{
  RegisterDebugLogLineHandler registers a line-based debug handler.

  @param(AHandler is a handling procedure.)
}
procedure RegisterDebugLogLineHandler(AHandler: TDebugLogLineHandler);

{
  RemoveDebugLogDataHandler removes a line-based debug handler.

  @param(AHandler is a handling procedure.)
}
procedure RemoveDebugLogLineHandler(AHandler: TDebugLogLineHandler);

{
  ExitsDebugLogLineHandler tests whether a line-based debug handler is registered.

  @param(AHandler is a handling procedure.)
}
function IsRegisteredDebugLogLineHandler(AHandler: TDebugLogLineHandler): boolean;

{
  RegisterDebugLogDataHandler registers a data object-based debug handler.

  @param(AHandler is a handling procedure.)
}
procedure RegisterDebugLogDataHandler(AHandler: TDebugLogDataHandler);

{
  RemoveDebugLogDataHandler removes a line-based debug handler.

  @param(AHandler is a handling procedure.)
}
procedure RemoveDebugLogDataHandler(AHandler: TDebugLogDataHandler);

{
  SendDebugLogLineToHandlers sends a debug line to all registered line handlers.

  @param(ALine is a text line with debug information.)
}
procedure SendDebugLogLineToHandlers(const ALine: WideString);

{
  SendDebugLogLineToHandlers sends debug data to all registered data handlers.
}
procedure SendDebugLogDataToHandlers(ADirection: TDebugDirection; AType: TDebugType; AObject: TObject; AText: WideString; const AArgs: array of const);

implementation

uses
   Firefly.Streams,
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   DebugLog.Destinations.Console;

type
   TDebugStreamField = (dstProcessID, dstThreadID, dstTimestamp, dstMessage, dstUnit, dstClass, dstType, dstDirection, dstLevel);
   TDebugStreamFieldType = cardinal;

{ TDebugMessage }

constructor TDebugMessage.Create(AMessageText: WideString; AObject: TObject; AType: TDebugType; ADirection: TDebugDirection; ALevel: TDebugLevel);
begin
   FData.MessageText := AMessageText;
   FData.AffectedObject := AObject;
   if Assigned(FData.AffectedObject) then begin
      {$IFNDEF SkipIncompatibleParts}
      {$IFDEF FPC}
      FData.UnitName := UTF8Decode(FData.AffectedObject.UnitName);
      {$ELSE FPC}
      FData.UnitName := FData.AffectedObject.UnitName;
      {$ENDIF FPC}
      {$ENDIF SkipIncompatibleParts}
      {$IFDEF FPC}
      FData.ClassName := UTF8Decode(FData.AffectedObject.ClassName);
      {$ELSE FPC}
      FData.ClassName := FData.AffectedObject.ClassName;
      {$ENDIF FPC}
   end;
   FData.MessageType := AType;
   FData.Direction := ADirection;
   FData.Level := ALevel;
   {$IFDEF Windows}
   FData.ProcessID := GetCurrentProcessId;
   FData.ThreadID := GetCurrentThreadId;
   {$ENDIF Windows}
   FData.Timestamp := Now;
end;

constructor TDebugMessage.Create(AStream: TStream);
var
   dstValue: TDebugStreamFieldType;
   dst: TDebugStreamField;
begin
   {$IFDEF MSWindows}
   ZeroMemory(@dstValue, SizeOf(dstValue));
   {$ELSE MSWindows}
   FillChar(dstValue, SizeOf(dstValue), 0);
   {$ENDIF MSWindows}
   while AStream.Position < AStream.Size do begin
      AStream.Read(dstValue, SizeOf(dstValue));
      dst := TDebugStreamField(dstValue);
      case dst of
         dstProcessID: FData.ProcessID := AStream.ReadDWord;
         dstThreadID: FData.ThreadID := AStream.ReadDWord;
         dstTimestamp: FData.Timestamp := AStream.ReadDateTime;
         dstMessage: FData.MessageText := AStream.ReadUTF8WideString;
         dstUnit: FData.UnitName := AStream.ReadUTF8WideString;
         dstClass: FData.ClassName := AStream.ReadUTF8WideString;
         dstType: FData.MessageType := TDebugType(AStream.ReadDWord);
         dstDirection: FData.Direction := TDebugDirection(AStream.ReadDWord);
         dstLevel: FData.Level := TDebugLevel(AStream.ReadDWord);
      end;
   end;
end;

 //class function TDebugMessage.PostToPipe(AMessageText: WideString; AObject: TObject; AType: TDebugType; ADirection: TDebugDirection; ALevel: TDebugLevel): boolean;
 //var
 //   ms: TMemoryStream;
 //begin
 //   Result := False;
 //   if Assigned(AObject) then begin
 //      {$IFNDEF SkipIncompatibleParts}
 //      if AObject.UnitName = 'snlPipes' then begin
 //         Exit;
 //      end;
 //      {$ENDIF SkipIncompatibleParts}
 //   end;
 //   with Self.Create(AMessageText, AObject, AType, ADirection, ALevel) do begin
 //      ms := TMemoryStream.Create;
 //      try
 //         ToStream(ms);
 //         TStreamPipeClient.SendSingleStream(DebugPipeName, 'snlDebug', ms);
 //      finally
 //         FreeAndNil(ms);
 //         Free;
 //      end;
 //   end;
 //end;

procedure TDebugMessage.ToStream(AStream: TStream);
begin
   AStream.WriteTypedDWord(FData.ProcessID, TDebugStreamFieldType(dstProcessID));
   AStream.WriteTypedDWord(FData.ThreadID, TDebugStreamFieldType(dstThreadID));
   AStream.WriteTypedDWord(DWord(FData.MessageType), TDebugStreamFieldType(dstType));
   AStream.WriteTypedDWord(DWord(FData.Direction), TDebugStreamFieldType(dstDirection));
   AStream.WriteTypedDWord(DWord(FData.Level), TDebugStreamFieldType(dstLevel));
   AStream.WriteTypedDateTime(FData.Timestamp, TDebugStreamFieldType(dstTimestamp));
   AStream.WriteTypedUTF8String(UTF8Encode(FData.MessageText), TDebugStreamFieldType(dstMessage));
   if Assigned(FData.AffectedObject) then begin
      {$IFNDEF SkipIncompatibleParts}
      AStream.WriteTypedUTF8String(UTF8Encode(FData.AffectedObject.UnitName), TDebugStreamFieldType(dstUnit));
      {$ENDIF SkipIncompatibleParts}
      AStream.WriteTypedUTF8String(UTF8Encode(FData.AffectedObject.ClassName), TDebugStreamFieldType(dstClass));
   end;
end;

procedure TDebugMessage.ToStringList(AList: TStrings);
var
   sDirection: string;
   sType: string;
begin
   AList.Clear;
   {$IFDEF Unicode}
   AList.Add('message=' + FData.MessageText);
   {$ELSE Unicode}
   AList.Add('message=' + UTF8Encode(FData.MessageText));
   {$ENDIF Unicode}
   if Assigned(FData.AffectedObject) then begin
      {$IFNDEF SkipIncompatibleParts}
      AList.Add('unit=' + FData.AffectedObject.UnitName);
      {$ENDIF SkipIncompatibleParts}
      AList.Add('class=' + FData.AffectedObject.ClassName);
   end;
   sDirection := '?';
   case FData.Direction of
      dEnter: sDirection := '>';
      dLeave: sDirection := '<';
      dMessage: sDirection := 'i';
      dWarning: sDirection := '!';
      dError: sDirection := '!';
   end;
   AList.Add('direction=' + sDirection);
   sType := '';
   case FData.MessageType of
      dtProcess: sType := 'process';
      dtThread: sType := 'thread';
      dtMethod: sType := 'method';
      dtUnitConstructor: sType := 'unit initialization';
      dtUnitDestructor: sType := 'unit finalization';
      else
         sType := '';
   end;
   AList.Add('type=' + sType);
   AList.Add('timestamp=' + FormatDateTime('yyyy-mm-dd hh:nn:ss', FData.Timestamp));
end;

{ TDebugMessageData }

procedure TDebugMessageData.Assign(ASource: TDebugMessageData);
begin
   Self.ThreadID := ASource.ThreadID;
   Self.ProcessID := ASource.ProcessID;
   Self.Level := ASource.Level;
   Self.Direction := ASource.Direction;
   Self.MessageType := ASource.MessageType;
   Self.MessageText := ASource.MessageText;
   Self.AffectedObject := ASource.AffectedObject;
   Self.UnitName := ASource.UnitName;
   Self.ClassName := ASource.ClassName;
   Self.Timestamp := ASource.Timestamp;
end;

function TDebugMessageData.ObjectPrefix: string;
begin
   // in FreePascal, UnitName and ClassName are WideString.
   // Result depends on Unicode switch.
   if Assigned(AffectedObject) then begin
      {$IFDEF Unicode}
      Result := UnitName + '.' + ClassName + '.';
      {$ELSE Unicode}
      Result := UTF8Encode(UnitName) + '.' + UTF8Encode(ClassName) + '.';
      {$ENDIF Unicode}
   end else begin
      Result := '';
   end;
end;

function TDebugMessageData.ToLine(ALastMessageTimestamp: TDateTime): WideString;
begin
   Result := WideString(Format('%4x:%4x [%s +%s] %s [%s] %s%s', [ProcessID, ThreadID, FormatDateTime('nn:ss.zzz', Timestamp), FormatDateTime('nn:ss.zzz',
      Timestamp - ALastMessageTimestamp), SDebugDirection[Direction], SDebugType[MessageType], ObjectPrefix, MessageText]));
end;

type

   {
     THandlerData handles various debug handlers.
   }
   THandlerData = class
   private
      FLineHandlers: TList<TDebugLogLineHandler>;
      FDataHandlers: TList<TDebugLogDataHandler>;
   protected
      class var FInstance: THandlerData;
   public
      class constructor Create;
      class destructor Destroy;
      class function Instance: THandlerData;
   public
      constructor Create;
      destructor Destroy; override;
      procedure RegisterDebugLogLineHandler(AHandler: TDebugLogLineHandler);
      procedure RegisterDebugLogDataHandler(AHandler: TDebugLogDataHandler);
      function IsRegisteredDebugLogLineHandler(AHandler: TDebugLogLineHandler): boolean;
      procedure RemoveDebugLogLineHandler(AHandler: TDebugLogLineHandler);
      procedure RemoveDebugLogDataHandler(AHandler: TDebugLogDataHandler);
      procedure SendDebugLogLineToHandlers(const ALine: WideString);
      procedure SendDebugLogDataToHandlers(ADirection: TDebugDirection; AType: TDebugType; AObject: TObject; AText: WideString; const AArgs: array of const);
   end;


function HandlerData: THandlerData;
begin
   Result := THandlerData.Instance;
end;

procedure RegisterDebugLogLineHandler(AHandler: TDebugLogLineHandler);
begin
   HandlerData.RegisterDebugLogLineHandler(AHandler);
end;

function IsRegisteredDebugLogLineHandler(AHandler: TDebugLogLineHandler): boolean;
begin
   Result := HandlerData.IsRegisteredDebugLogLineHandler(AHandler);
end;

procedure RegisterDebugLogDataHandler(AHandler: TDebugLogDataHandler);
begin
   HandlerData.RegisterDebugLogDataHandler(AHandler);
end;

procedure RemoveDebugLogDataHandler(AHandler: TDebugLogDataHandler);
begin
   HandlerData.RemoveDebugLogDataHandler(AHandler);
end;

procedure RemoveDebugLogLineHandler(AHandler: TDebugLogLineHandler);
begin
   HandlerData.RemoveDebugLogLineHandler(AHandler);
end;

procedure SendDebugLogLineToHandlers(const ALine: WideString);
begin
   HandlerData.SendDebugLogLineToHandlers(ALine);
end;

procedure SendDebugLogDataToHandlers(ADirection: TDebugDirection; AType: TDebugType; AObject: TObject; AText: WideString; const AArgs: array of const);
begin
   HandlerData.SendDebugLogDataToHandlers(ADirection, AType, AObject, AText, AArgs);
end;

{ THandlerData }

destructor THandlerData.Destroy;
begin
   FLineHandlers.Free;
   FDataHandlers.Free;
   inherited;
end;

procedure THandlerData.RegisterDebugLogDataHandler(AHandler: TDebugLogDataHandler);
begin
   FDataHandlers.Add(AHandler);
end;

function THandlerData.IsRegisteredDebugLogLineHandler(AHandler: TDebugLogLineHandler): boolean;
begin
   Result := FLineHandlers.IndexOf(AHandler) > -1;
end;

procedure THandlerData.RemoveDebugLogLineHandler(AHandler: TDebugLogLineHandler);
begin
   FLineHandlers.Remove(AHandler);
end;

procedure THandlerData.RemoveDebugLogDataHandler(AHandler: TDebugLogDataHandler);
begin
   FDataHandlers.Remove(AHandler);
end;

procedure THandlerData.RegisterDebugLogLineHandler(AHandler: TDebugLogLineHandler);
begin
   FLineHandlers.Add(AHandler);
end;

procedure THandlerData.SendDebugLogDataToHandlers(ADirection: TDebugDirection; AType: TDebugType; AObject: TObject; AText: WideString; const AArgs: array of const);
var
   h: TDebugLogDataHandler;
begin
   for h in FDataHandlers do begin
      h(ADirection, AType, AObject, AText, AArgs);
   end;
end;

procedure THandlerData.SendDebugLogLineToHandlers(const ALine: WideString);
var
   h: TDebugLogLineHandler;
begin
   for h in FLineHandlers do begin
      h(ALine);
   end;
end;

class constructor THandlerData.Create;
begin
  FInstance := nil;
end;

class destructor THandlerData.Destroy;
begin
   FInstance.Free;
end;

class function THandlerData.Instance: THandlerData;
begin
   if not Assigned(FInstance) then begin
      FInstance := THandlerData.Create;
   end;
   Result := FInstance;
end;

constructor THandlerData.Create;
begin
   FLineHandlers := TList<TDebugLogLineHandler>.Create;
   FDataHandlers := TList<TDebugLogDataHandler>.Create;
end;

initialization

   DebugOutputs := [];
   {$IFDEF DEBUG}
   DebugOutputs := [];
   if FindCmdLineSwitch('debuglogfile') then begin
      DebugOutputs := DebugOutputs + [doLogFile];
   end;
   if FindCmdLineSwitch('debugeventlog') then begin
      DebugOutputs := DebugOutputs + [doEventLog];
   end;
   if FindCmdLineSwitch('debugconsole') then begin
      DebugOutputs := DebugOutputs + [doConsole];
   end;
   if FindCmdLineSwitch('debugwinconsole') then begin
      DebugOutputs := DebugOutputs + [doDebugConsole];
   end;
   {$ENDIF DEBUG}

end.
