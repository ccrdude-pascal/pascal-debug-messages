{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Measures code performance.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2000-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-03-21  pk  10m  Found error in TMeasurePoint constructor
// 2017-03-21  pk  10m  Fixed access to RuntimeManager object by wrapping it in a function
// 2017-02-28  --  ---  Renamed from snlCodePerformance to PepiMK.Debug.CodePerformance
// 2010-10-11  ##   --  Beautified code (using Code Beautifier Wizard)
// 2010-10-11  ##   --  Added this header (from Code Beautifier Wizard)
// *****************************************************************************
// ;RuntimePoints;RuntimePointsVisible
   )
}

unit DebugLog.CodePerformance;

{$J+}

interface

uses
   SysUtils,
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   Classes;

type
   TMeasureList = class;

   TMeasurePoint = class(TCollectionItem)
   private
      FOwner: TMeasureList;
      FPercentage: real;
      FText: string;
      FTime: TDateTime;
      function GetDiffTime: TDateTime;
   protected
      function GetDisplayName: string; override;
   public
      constructor Create(ACollection: TCollection); override;
      destructor Destroy; override;
      procedure Assign(Source: TPersistent); override;
      function ToLine: string;
   published
      property DiffTime: TDateTime read GetDiffTime;
      property Percentage: real read FPercentage write FPercentage;
      property Text: string read FText write FText;
      property Time: TDateTime read FTime write FTime;

   end;

   TMeasureList = class(TCollection)
   private
      function GetItem(Index: integer): TMeasurePoint;
      procedure SetItem(Index: integer; Value: TMeasurePoint);
      procedure RePercentage;
      function EndLine: string;
   public
      constructor Create;
      destructor Destroy; override;
      function Add: TMeasurePoint;
      procedure SaveToFile(AFilename: string);
      property Items[Index: integer]: TMeasurePoint read GetItem write SetItem; default;
   end;

   TRuntimeManager = class
   private
      FMeasureList: TMeasureList;
      FShowDebugMessages: boolean;
      FCreated: TDateTime;
   public
      constructor Create;
      destructor Destroy; override;
      function AddPoint(AText: string): TMeasurePoint;
      procedure DumpFile(AFilename: string); overload;
      procedure DumpFile; overload;
      procedure DumpFile2;
      property MeasureList: TMeasureList read FMeasureList;
      property ShowDebugMessages: boolean read FShowDebugMessages write FShowDebugMessages;
   end;

procedure AddRunTimeManagerPoint(APointDescription: string);
procedure InitRunTimeManager;
function RunTimeManager: TRuntimeManager;

implementation

uses
   DebugLog;

var
   FRunTimeManager: TRuntimeManager;

const
   GTimeFormat           = 'hh:nn:ss:zzz';
   GPointCount: cardinal = 0;
   {$IFDEF RuntimePointsVisible}
   GShowPoints: boolean  = True;

   {$ENDIF RuntimePointsVisible}

procedure AddRunTimeManagerPoint(APointDescription: string);
{$IFDEF RuntimePointsVisible}
var
   mp: TMeasurePoint;
   {$ENDIF RuntimePointsVisible}
begin
   try
      if Assigned(RunTimeManager) then begin
         {$IFDEF RuntimePointsVisible}
         mp :=
            {$ENDIF}
            RunTimeManager.AddPoint(APointDescription);
         {$IFDEF MSWindows}
         OutputDebugString(PChar(APointDescription));
         {$ENDIF MSWindows}
         Inc(GPointCount);
         {$IFDEF RuntimePointsVisible}
         if GShowPoints then begin
            // MessageBox(0, PChar('[#' + IntToStr(GPointCount) + ']' + #13#10 + APointDescription), 'snlCodePerformance', 0);
            {$IFDEF ConsoleWindow}
            WriteLn(mp.ToLine);
            {$ELSE ConsoleWindow}
            if MessageDlg('snlCodePerformance: [#' + IntToStr(GPointCount) + ']' + #13#10 + APointDescription, mtInformation, [mbOK, mbIgnore], 0) = mrIgnore then begin
               GShowPoints := False;
            end;
            {$ENDIF ConsoleWindow}
         end;
         {$ENDIF RuntimePointsVisible}
      end;
   except
      // during shutdown, this shouldn't be used any longer!
   end;
end;

procedure InitRunTimeManager;
begin
   if not Assigned(FRunTimeManager) then begin
      FRunTimeManager := TRuntimeManager.Create;
      AddRunTimeManagerPoint('InitRunTimeManager');
   end;
end;

function RunTimeManager: TRuntimeManager;
begin
   if not Assigned(FRunTimeManager) then begin
      InitRunTimeManager;
   end;
   Result := FRunTimeManager;
end;

procedure TMeasurePoint.Assign(Source: TPersistent);
begin
   if Source is TMeasurePoint then
      with Source as TMeasurePoint do begin
         Self.FOwner := FOwner;
         Self.FPercentage := FPercentage;
         Self.FText := FText;
         Self.FTime := FTime;
      end else
      inherited Assign(Source);
end;

constructor TMeasurePoint.Create(ACollection: TCollection);
begin
   inherited Create(ACollection);
   FOwner := TMeasureList(ACollection);
   FTime := Now;
end;

destructor TMeasurePoint.Destroy;
begin
   inherited Destroy;
end;

function TMeasurePoint.GetDiffTime: TDateTime;
begin
   try
      if Index > 0 then
         Result := FTime - FOwner.Items[Index - 1].FTime
      else
         Result := 0;
   except
      Result := 0;
   end;
end;

function TMeasurePoint.GetDisplayName: string;
begin
   Result := FText;
   if Result = '' then
      Result := inherited GetDisplayName;
end;

function TMeasureList.Add: TMeasurePoint;
begin
   Result := TMeasurePoint(inherited Add);
end;

constructor TMeasureList.Create;
begin
   inherited Create(TMeasurePoint);
end;

destructor TMeasureList.Destroy;
begin
   inherited;
end;

function TMeasureList.EndLine: string;
var
   timeDiff, timeSum: TDateTime;
   i: integer;
   percSum: real;
begin
   Result := '';
   if Count > 1 then begin
      timeDiff := Items[Count - 1].Time - Items[0].Time;
      timeSum := 0;
      for i := 0 to Count - 1 do
         timeSum := timeSum + Items[i].DiffTime;
      percSum := 0;
      for i := 0 to Count - 1 do
         percSum := percSum + Items[i].Percentage;
      Result := FormatDateTime(GTimeFormat, timeDiff) + ' ' + FormatDateTime(GTimeFormat, timeSum) + ' ' + Format('%5.1f', [percSum * 100]) + '%';
   end;
end;

function TMeasureList.GetItem(Index: integer): TMeasurePoint;
begin
   Result := TMeasurePoint(inherited GetItem(Index));
end;

procedure TMeasureList.RePercentage;
var
   i: integer;
   fullTime: TDateTime;
begin
   if Count > 0 then begin
      Items[0].Percentage := 0;
   end;
   if (Count > 1) then begin
      fullTime := Items[Count - 1].Time - Items[0].Time;
      if (fullTime > 0) then begin
         for i := 1 to Pred(Count) do begin
            Items[i].Percentage := (Items[i].DiffTime) / fullTime;
         end;
      end;
   end;
end;

procedure TMeasureList.SaveToFile(AFilename: string);
var
   i: integer;
   sl: TStringList;
begin
   RePercentage;
   sl := TStringList.Create;
   try
      for i := 0 to Pred(Count) do
         sl.Add(Items[i].ToLine);
      sl.Add('------------ ------------ ------');
      sl.Add(EndLine);
      sl.SaveToFile(AFilename);
   finally
      sl.Free;
   end;
end;

procedure TMeasureList.SetItem(Index: integer; Value: TMeasurePoint);
begin
   inherited SetItem(Index, Value);
end;

{ TRuntimeManager }

function TRuntimeManager.AddPoint(AText: string): TMeasurePoint;
begin
   Result := FMeasureList.Add;
   Result.Text := AText;
   if FShowDebugMessages then begin
      {$IFDEF MSWindows}
      OutputDebugString(PChar(AText));
      {$ENDIF MSWindows}
   end;
   DumpFile2;
end;

constructor TRuntimeManager.Create;
begin
   inherited;
   FMeasureList := TMeasureList.Create;
   FShowDebugMessages := False;
   FCreated := Now;
end;

destructor TRuntimeManager.Destroy;
begin
   FMeasureList.Free;
   inherited;
end;

procedure TRuntimeManager.DumpFile(AFilename: string);
begin
   FMeasureList.SaveToFile(AFilename);
end;

function TMeasurePoint.ToLine: string;
begin
   Result := FormatDateTime(GTimeFormat, FTime) + ' ' + FormatDateTime(GTimeFormat, GetDiffTime) + ' ' + Format('%5.1f', [FPercentage * 100]) + '% ' + FText;
end;

procedure TRuntimeManager.DumpFile;
begin
   DumpFile('c:\' + FormatDateTime('hhnnss', Now) + '.txt');
end;

procedure TRuntimeManager.DumpFile2;
begin
   DumpFile(ParamStr(0) + '.runtime');
end;

initialization
   DebugLogger.LogEnterUnitInitialization('DebugLog.CodePerformance');
try
   {$IFDEF RuntimePoints}
   InitRunTimeManager;
   {$ELSE RuntimePoints}
   FRunTimeManager := nil;
   {$ENDIF RuntimePoints}
finally
   DebugLogger.LogLeaveUnitInitialization('DebugLog.CodePerformance');
end;

finalization
   DebugLogger.LogEnterUnitFinalization('DebugLog.CodePerformance');
try
   {$IFDEF RuntimePoints}
   FRunTimeManager.Free;
   {$ENDIF RuntimePoints}
finally
   DebugLogger.LogLeaveUnitFinalization('DebugLog.CodePerformance');
end;
end.
