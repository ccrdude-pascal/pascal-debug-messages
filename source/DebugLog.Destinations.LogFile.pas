﻿{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Forwards debug messages to a log file.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2010-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2018-04-10  pk   5m  Now tests of doLogFile is set
// 2017-03-21  pk   5m  Changed log filename
// *****************************************************************************
   )
}

unit DebugLog.Destinations.LogFile;

{$IFDEF FPC}
{$MODE Delphi}
{$ENDIF FPC}

interface

{
  This handler gets automatically registered as a destination for debug lines.
  It outputs debug information to a text file.

  @param(ALine is the text it should output.)
}
procedure SendDebugLogLineToLogFile(const ALine: WideString);
function GetDebugLogFilename: string;

implementation

uses
   SysUtils, // FileExists
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   Classes, // TFileStream
   SyncObjs,
   DebugLog.Consts,
   DebugLog.Destinations.DebugConsole;

var
   SNLDebugCriticalSection: TCriticalSection;
   SNLDebugLogFilename: string;

   {
     This handler gets automatically registered as a destination for debug lines.
     It outputs debug information to the console.

     @param(ALine is the text it should output.)
   }
procedure SendDebugLogLineToLogFile(const ALine: WideString);
var
   fs: TFileStream;
   sLine: rawbytestring;
begin
   if not (doLogFile in DebugOutputs) then begin
      Exit;
   end;
   if Assigned(SNLDebugCriticalSection) then begin
      SNLDebugCriticalSection.Enter;
   end;
   try
      try
         if FileExists(SNLDebugLogFilename) then begin
            fs := TFileStream.Create(SNLDebugLogFilename, fmOpenWrite or fmShareDenyNone);
         end else begin
            fs := TFileStream.Create(SNLDebugLogFilename, fmCreate or fmShareDenyNone);
         end;
         try
            fs.Seek(0, soEnd);
            sLine := rawbytestring(utf8string(ALine)) + #13#10;
            fs.Write(sLine[1], Length(sLine));
         finally
            fs.Free;
         end;
      except
         on E: Exception do begin
            {$IFDEF MSWindows}
            OutputDebugStringW(pwidechar(WideString('SendDebugLogLineToLogFile: ' + E.Message)));
            {$ENDIF MSWindows}
         end;
      end;
   finally
      if Assigned(SNLDebugCriticalSection) then begin
         SNLDebugCriticalSection.Leave;
      end;
   end;
end;

function GetDebugLogFilename: string;
begin
   Result := SNLDebugLogFilename;
end;

initialization

   SNLDebugCriticalSection := TCriticalSection.Create;
   SNLDebugLogFilename := GetModuleName(HInstance) + '.log';
   RegisterDebugLogLineHandler(SendDebugLogLineToLogFile);

finalization

   SNLDebugCriticalSection.Free;
   // setting it to nil is essential for
   SNLDebugCriticalSection := nil;

end.
