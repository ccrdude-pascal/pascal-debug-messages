﻿{
  @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
  @abstract(Forwards debug messages to the debug console.)

  @preformatted(
  // *****************************************************************************
  // Copyright: © 2010-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
  // *****************************************************************************
  // Changelog (new entries first):
  // ---------------------------------------
  // *****************************************************************************
  )
}

unit DebugLog.Destinations.DebugConsole;

{$IFDEF FPC}
{$MODE Delphi}
{$ELSE FPC}
{$DEFINE Windows}
{$ENDIF FPC}

interface

{
  This handler gets automatically registered as a destination for debug lines.
  It outputs debug information to the debug console.

  @param(ALine is the text it should output.)
}
procedure SendDebugLogLineToDebugConsole(const ALine: WideString);

implementation

uses
   {$IFDEF Windows}
   Windows,
   {$ENDIF Windows}
   DebugLog.Consts;

procedure SendDebugLogLineToDebugConsole(const ALine: WideString);
begin
   if not(doDebugConsole in DebugOutputs) then begin
      Exit;
   end;
   {$IFDEF Windows}
   OutputDebugStringW(PWideChar(ALine));
   {$ENDIF Windows}
end;

initialization

RegisterDebugLogLineHandler(SendDebugLogLineToDebugConsole);

end.
