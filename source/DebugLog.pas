﻿{
  @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
  @abstract(Handles debug messages.)

  @preformatted(
// *****************************************************************************
// Copyright: © 2010-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2011-02-11  ##   --  Beautified code (using Code Beautifier Wizard)
// 2010-07-22  pk   --  Added this header (from Code Test Browser)
// *****************************************************************************
// use /debuglogfile                   to write to appname.exe.log
// use /logfile=c:\log.txt             to write to custom log file
// use /debugnetconsole                to write to local console over tcp
// use /debugnetconsole=192.168.1.10   to write to network console over tcp
// use /debuglevel=debug               to see everything
// use /debuglevel=message             to see coarse-grained details
// use /debuglevel=verbose             to see fine-grained details
// *****************************************************************************
// Available compiler switches for various output methods:
// $DEFINE DebugConsole
// $DEFINE DebugLogFile
// $IFDEF ForceDebugLogFile
// $IFDEF ForceDebugConsole
// *****************************************************************************
)
}

unit DebugLog;

interface

{$IFDEF DEBUG}
{$DEFINE DebugConsole}
{$DEFINE DebugLogFile}
{$IFNDEF FPC}
{$IFDEF MSWindows}
{$DEFINE DebugShowStackTrace}
{$DEFINE DebugExceptionVector}
{$ENDIF MSWindows}
{$ENDIF FPC}
{$ELSE DEBUG}
{$UNDEF DebugConsole}
{$UNDEF DebugLogFile}
{$UNDEF DebugExceptionVector}
{$DEFINE DebugCodeInline}
{$UNDEF DebugShowStackTrace}
{$ENDIF DEBUG}
{$IFNDEF FPC}
{$UNDEF DebugExceptionVector}// just because GetModuleHandleEx is not available right away
{$ENDIF FPC}
{$UNDEF DebugCodeInline}

uses
   SysUtils,
   {$IFDEF MSWindows} Windows, {$ENDIF MSWindows}
   {$IFDEF DebugShowStackTrace} madStackTrace, {$ENDIF DebugShowStackTrace}
   // {$IFDEF DebugExceptionVector} PepiMK.Windows.ErrorReporting, {$ENDIF DebugExceptionVector}
   Classes,
   SyncObjs,
   {$IFDEF DEBUG}
   PepiMK.Console, // used for HasParamSwitch for example
   {$ENDIF DEBUG}
   DebugLog.Consts;

type

   { TDebugLogger }

   TDebugLogger = class(TMultiReadExclusiveWriteSynchronizer)
   private
      FDebugLevelInDebugMode: TDebugLevel;
      FDebugLevelInReleaseMode: TDebugLevel;
      FUseConsole: boolean;
      FUseDebugConsole: boolean;
      FUseLogFile: boolean;
      FUseEventLog: boolean;
      FLogFilename: WideString;
      FProcessName: WideString;
      function TestLevel(ALevel: TDebugLevel): boolean;
      function GetDebugLevelInDebugMode: TDebugLevel;
      function GetDebugLevelInReleaseMode: TDebugLevel;
      procedure SetDebugLevelInDebugMode(const Value: TDebugLevel);
      procedure SetDebugLevelInReleaseMode(const Value: TDebugLevel);
      function GetUseConsole: boolean;
      procedure SetUseConsole(const Value: boolean);
      function GetUseDebugConsole: boolean;
      procedure SetUseDebugConsole(const Value: boolean);
      function GetUseLogFile: boolean;
      procedure SetUseLogFile(const Value: boolean);
      function GetUseEventLog: boolean;
      procedure SetUseEventLog(const Value: boolean);
      function GetLogFilename: WideString;
      procedure SetLogFilename(const Value: WideString);
   protected
      class var FInstance: TDebugLogger;
      function TestUseConsole: boolean;
      procedure LogMessageInt(ADirection: TDebugDirection; AType: TDebugType; AObject: TObject; AText: WideString; const AArgs: array of const; ALevel: TDebugLevel); overload;
      procedure LogMessageInt(ADirection: TDebugDirection; AType: TDebugType; AObject: TObject; AText: ansistring; const AArgs: array of const; ALevel: TDebugLevel); overload;
      procedure LogVariableInt( {%H-}AVariableName, {%H-}AVariableData: WideString; {%H-}ALevel: TDebugLevel); overload;
      procedure LogVariableInt(AVariableName, AVariableData: ansistring; ALevel: TDebugLevel); overload;
   public
       class constructor Create;
       class destructor Destroy;
       class function Instance: TDebugLogger;
   public
      constructor Create; {$IFDEF FPC} override; {$ENDIF FPC}
      procedure LogEnterProcess(AProcessName: ansistring); overload;
      procedure LogEnterProcess(AProcessName: WideString); overload;
      procedure LogLeaveProcess(AProcessName: ansistring); overload;
      procedure LogLeaveProcess(AProcessName: WideString); overload;
      procedure LogEnterThread(AThreadName: WideString; ALevel: TDebugLevel = dlvControl); overload;
      procedure LogEnterThread(AThread: TThread; AThreadName: WideString; ALevel: TDebugLevel = dlvControl); overload;
      procedure LogLeaveThread(AThreadName: WideString; ALevel: TDebugLevel = dlvControl); overload;
      procedure LogLeaveThread(AThread: TThread; AThreadName: WideString; ALevel: TDebugLevel = dlvControl); overload;
      procedure LogEnterMethod(AObject: TObject; AMethodName: WideString; ALevel: TDebugLevel = dlvDefault); overload;
      procedure LogEnterMethod(AObject: TObject; AMethodName: ansistring; const AArgs: array of const; ALevel: TDebugLevel = dlvDefault); overload;
      procedure LogEnterMethod(AObject: TObject; AMethodName: WideString; const AArgs: array of const; ALevel: TDebugLevel = dlvDefault); overload;
      procedure LogEnterMethod(AMethodName: WideString; ALevel: TDebugLevel = dlvDefault); overload;
      procedure LogEnterMethod(AMethodName: WideString; const AArgs: array of const; ALevel: TDebugLevel = dlvDefault); overload;
      procedure LogLeaveMethod(AObject: TObject; AMethodName: WideString; ALevel: TDebugLevel = dlvDefault); overload;
      procedure LogLeaveMethod(AObject: TObject; AMethodName: WideString; const AArgs: array of const; ALevel: TDebugLevel = dlvDefault); overload;
      procedure LogLeaveMethod(AMethodName: WideString; const AArgs: array of const; ALevel: TDebugLevel = dlvDefault); overload;
      procedure LogLeaveMethod(AMethodName: WideString; ALevel: TDebugLevel = dlvDefault); overload;
      procedure LogEnterUnitInitialization(AUnitName: WideString);
      procedure LogLeaveUnitInitialization(AUnitName: WideString);
      procedure LogEnterUnitFinalization(AUnitName: WideString);
      procedure LogLeaveUnitFinalization(AUnitName: WideString);
      procedure LogMessage(AMessage: ansistring; ALevel: TDebugLevel = dlvDefault); overload;
      procedure LogMessage(AMessage: WideString; ALevel: TDebugLevel = dlvDefault); overload;
      procedure LogMessage(AObject: TObject; AMessage: ansistring; ALevel: TDebugLevel = dlvDefault); overload;
      procedure LogMessage(AObject: TObject; AMessage: WideString; ALevel: TDebugLevel = dlvDefault); overload;
      procedure LogWarning(AWarnText: string);
      procedure LogError(AErrorMessage: ansistring; ALevel: TDebugLevel = dlvError; AObject: TObject = nil); overload;
      procedure LogError(AErrorMessage: WideString; ALevel: TDebugLevel = dlvError; AObject: TObject = nil); overload;
      procedure LogError(AErrorCode: cardinal; AErrorMessage: string; ALevel: TDebugLevel = dlvError); overload;
      procedure LogException(AException: Exception; AMessage: string; ALevel: TDebugLevel = dlvFatal); overload;
      procedure LogException(AException: Exception; AObject: TObject; AMessage: string; ALevel: TDebugLevel = dlvFatal); overload;
      {$IFDEF Windows} procedure LogException(AException: TExceptionPointers; AMessage: string; ALevel: TDebugLevel = dlvFatal); overload; {$ENDIF}
      procedure LogVarString(AName: ansistring; AVar: ansistring; ALevel: TDebugLevel = dlvDebug); overload; {$IFDEF DebugCodeInline} inline; {$ENDIF}
      procedure LogVarString(AName: WideString; AVar: WideString; ALevel: TDebugLevel = dlvDebug); overload; {$IFDEF DebugCodeInline} inline; {$ENDIF}
      procedure LogVarUInt64(AName: string; AVar: uint64; ALevel: TDebugLevel = dlvDebug); overload; {$IFDEF DebugCodeInline} inline; {$ENDIF}
      procedure LogVarInteger(AName: string; AVar: integer; ALevel: TDebugLevel = dlvDebug); {$IFDEF DebugCodeInline} inline; {$ENDIF}
      procedure LogVarInt64(AName: string; AVar: int64; ALevel: TDebugLevel = dlvDebug); {$IFDEF DebugCodeInline} inline; {$ENDIF}
      procedure LogVarCardinal(AName: string; AVar: cardinal; ALevel: TDebugLevel = dlvDebug); {$IFDEF DebugCodeInline} inline; {$ENDIF}
      procedure LogVarBoolean(AName: string; AVar: boolean; ALevel: TDebugLevel = dlvDebug); {$IFDEF DebugCodeInline} inline; {$ENDIF}
      procedure LogVarStringList(AName: string; AVar: TStrings; ALevel: TDebugLevel = dlvDebug); {$IFDEF DebugCodeInline} inline; {$ENDIF}
      property DebugLevelInDebugMode: TDebugLevel read GetDebugLevelInDebugMode write SetDebugLevelInDebugMode;
      property DebugLevelInReleaseMode: TDebugLevel read GetDebugLevelInReleaseMode write SetDebugLevelInReleaseMode;
      property UseConsole: boolean read GetUseConsole write SetUseConsole;
      property UseDebugConsole: boolean read GetUseDebugConsole write SetUseDebugConsole;
      property UseLogFile: boolean read GetUseLogFile write SetUseLogFile;
      property UseEventLog: boolean read GetUseEventLog write SetUseEventLog;
      property LogFilename: WideString read GetLogFilename write SetLogFilename;
      property ProcessName: WideString read FProcessName;
   end;

var
   SNLDebugLevel: TDebugLevel;
   SNLDefaultDebugLevel: TDebugLevel;
   SNLDebugLastPointTimestamp: TDateTime;
   SNLDebugUseLogFile: boolean;
   SNLDebugUseConsole: boolean;
   SNLDebugUseWindow: boolean;
   SNLDebugUsePipes: boolean;
   SNLDebugLogFilename: WideString;
   SNLDebugCriticalSection: TCriticalSection;
   {$IFDEF DEBUG}
   SNLDebugCommandLineParameters: TCLPGroup;

   {$ENDIF DEBUG}

function DebugLogger: TDebugLogger;

implementation

{$IFDEF DebugExceptionVector}
var
   GVectoredHandler: Pointer;
   {$ENDIF DebugExceptionVector}

function DebugLogger: TDebugLogger;
begin
   Result := TDebugLogger.Instance;
end;

function DebugLinePrefixInt: WideString; {$IFDEF DebugCodeInline} inline;
   {$ENDIF}
begin
   {$IFDEF Windows}
   Result := WideFormat('{%s} %4x:%4x [%s +%s]', [ExtractFilename(GetModuleName(HInstance)), GetCurrentProcessId, GetCurrentThreadId, FormatDateTime('hh:nn:ss.zzz', Now),
      FormatDateTime('nn:ss.zzz', Now - SNLDebugLastPointTimestamp)]);
   {$ELSE Windows}
   // TODO : DEBUG - implement proper output
   // Result := Format('{%s} %4d [%s +%s]', [ExtractFilename(GetModuleName(HInstance)), GetCurrentThreadId, FormatDateTime('hh:nn:ss.zzz', Now), FormatDateTime('nn:ss.zzz', Now - SNLDebugLastPointTimestamp)]);
   Result := Format('{%s}', [ExtractFilename(GetModuleName(HInstance))]);
   // Result := Format('{%s}', [ExtractFilename(ParamStr(0))]);
   {$ENDIF Windows}
end;

{$IFDEF DebugExceptionVector}

function DebugLogVectoredHandler(var ExceptionInfo: TExceptionPointers): cardinal; stdcall;
var
   h: HMODULE;
   cFilenameSize: cardinal;
   pcwFilename: pwidechar;
begin
   if GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS, ExceptionInfo.ExceptionRecord.ExceptionAddress, h) then begin
      cFilenameSize := 1024;
      GetMem(pcwFilename, cFilenameSize);
      try
         GetModuleFileNameW(h, pcwFilename, cFilenameSize);
         DebugLogger.LogException(ExceptionInfo, pcwFilename);
      finally
         FreeMem(pcwFilename);
      end;
   end else begin
      DebugLogger.LogException(ExceptionInfo, '');
   end;
   Result := EXCEPTION_CONTINUE_SEARCH;
end;

{$ENDIF DebugExceptionVector}

procedure snlDebugInitialization;
{$IFDEF DEBUG}
const
   SBackSlash: WideString = '\';
var
   s: ansistring;
   {$ENDIF DEBUG}
begin
   if FindCmdLineSwitch('debugconsole') then begin
      if IsConsole then begin
         WriteLn('[i] console debugging enabled');
      end;
   end;
   {$IFDEF DEBUG}
   SNLDebugCommandLineParameters := CommandLineParameters.Add;
   SNLDebugUsePipes := True;
   SNLDebugCriticalSection := TCriticalSection.Create;
   SNLDebugUseConsole := FindCmdLineSwitch('debugconsole');
   {$IFDEF ForceDebugConsole}
   SNLDebugUseConsole := True;
   {$ENDIF ForceDebugConsole}
   {$IFDEF DebugConsole}
   if SNLDebugUseConsole then begin
      {$IFDEF MSWindows}
      IsConsole := AllocConsole;
      {$ENDIF MSWindows}
      WriteLn('Allocating debug console...');
   end;
   {$ENDIF DebugConsole}
   SNLDebugUseWindow := FindCmdLineSwitch('debugwindow');
   {$IFDEF ForceDebugWindow}
   SNLDebugUseWindow := True;
   {$ENDIF ForceDebugWindow}
   SNLDebugUseLogFile := FindCmdLineSwitch('debuglogfile');
   {$IFDEF ForceDebugLogFile}
   SNLDebugUseLogFile := True;
   {$ENDIF ForceDebugLogFile}
   if HasParamSwitch('logfile', SNLDebugLogFilename) then begin
      if Pos(SBackSlash, SNLDebugLogFilename) = 0 then begin
         {$IFDEF FPC}
         SNLDebugLogFilename := ExtractFilePath(UTF8Decode(GetModuleName(HInstance))) + SNLDebugLogFilename;
         {$ELSE FPC}
         SNLDebugLogFilename := ExtractFilePath(GetModuleName(HInstance)) + SNLDebugLogFilename;
         {$ENDIF FPC}
      end;
   end else begin
      {$IFDEF FPC}
      SNLDebugLogFilename := UTF8Decode(GetModuleName(HInstance)) + '.debug.log';
      {$ELSE FPC}
      SNLDebugLogFilename := GetModuleName(HInstance) + '.debug.log';
      {$ENDIF FPC}
   end;
   {$IFDEF DebugExceptionVector}
   GVectoredHandler := AddVectoredExceptionHandler(0, @DebugLogVectoredHandler);
   {$ENDIF DebugExceptionVector}
   SNLDefaultDebugLevel := dlvMessage;
   SNLDebugLevel := dlvFatal;
   if HasParamSwitch('debuglevel', s) then begin
      if s = 'debug' then begin
         SNLDebugLevel := dlvDebug;
      end else if s = 'message' then begin
         SNLDebugLevel := dlvMessage;
      end else if s = 'verbose' then begin
         SNLDebugLevel := dlvVerbose;
      end;
   end;
   {$IFDEF ForceDebugLogFile}
   SNLDebugLevel := dlvDebug;
   {$ENDIF ForceDebugLogFile}
   {$IFDEF ForceDebugConsole}
   SNLDebugLevel := dlvDebug;
   {$ENDIF ForceDebugConsole}
   with SNLDebugCommandLineParameters do begin
      GroupName := 'Debugging';
      Description := '';
      Necessity := gnAllOptional;
      {$IFDEF DebugConsole}
      AddParameter('debugconsole', 'displays debug output in a separate console window');
      {$ENDIF}
      {$IFDEF DebugWindow}
      AddParameter('debugwindow', 'displays debug output in a separate window');
      {$ENDIF}
      {$IFDEF DebugLogFile}
      AddParameter('debuglogfile', 'logs debug output to file');
      AddParameter('logfile', 'name of log file').Syntax := 'logfile=<filename>';
      {$ENDIF}
      AddParameter('debuglevel', 'controls the amout of debug information logged').Syntax := 'debuglevel=debug';
      AddParameter('debuglevel', 'controls the amout of debug information logged').Syntax := 'debuglevel=message';
      AddParameter('debuglevel', 'controls the amout of debug information logged').Syntax := 'debuglevel=verbose';
   end;
   SNLDebugLastPointTimestamp := Now;
   {$ENDIF DEBUG}
end;

procedure snlDebugFinalization;
begin
   {$IFDEF DEBUG}
   {$IFDEF DebugConsole}
   if SNLDebugUseConsole then begin
      if FindCmdLineSwitch('pause') then begin
         ReadLn;
      end;
   end;
   {$ENDIF DebugConsole}
   SNLDebugCriticalSection.Free;
   {$IFDEF DebugExceptionVector}
   if Assigned(GVectoredHandler) then begin
      RemoveVectoredExceptionHandler(@GVectoredHandler);
   end;
   {$ENDIF DebugExceptionVector}
   {$ENDIF DEBUG}
end;

{ TDebugLogger }

constructor TDebugLogger.Create;
begin
   inherited;
   FDebugLevelInDebugMode := dlvMessage;
   FDebugLevelInReleaseMode := dlvError;
   {$IFDEF FPC}
   FLogFilename := UTF8Decode(GetModuleName(HInstance)) + '.log';
   {$ELSE FPC}
   FLogFilename := GetModuleName(HInstance) + '.log';
   {$ENDIF FPC}
   {$IFDEF Debug}
   FUseConsole := IsConsole;
   FUseLogFile := True;
   FUseEventLog := True;
   FUseDebugConsole := True;
   {$ELSE Debug}
   FUseConsole := False;
   FUseLogFile := False;
   FUseDebugConsole := False;
   FUseEventLog := True;
   {$ENDIF Debug}
end;

procedure TDebugLogger.LogEnterMethod(AObject: TObject; AMethodName: WideString; const AArgs: array of const; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dEnter, dtMethod, AObject, AMethodName, AArgs, ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogEnterMethod(AObject: TObject; AMethodName: WideString; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dEnter, dtMethod, AObject, AMethodName, [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogEnterMethod(AObject: TObject; AMethodName: ansistring; const AArgs: array of const; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dEnter, dtMethod, AObject, AMethodName, AArgs, ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogEnterMethod(AMethodName: WideString; const AArgs: array of const; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dEnter, dtMethod, nil, AMethodName, AArgs, ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogEnterProcess(AProcessName: ansistring);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      FProcessName := UTF8Decode(AProcessName);
      LogMessageInt(dEnter, dtProcess, nil, AProcessName, [], dlvControl);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogEnterProcess(AProcessName: WideString);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      FProcessName := AProcessName;
      LogMessageInt(dEnter, dtProcess, nil, AProcessName, [], dlvControl);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogEnterThread(AThreadName: WideString; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      LogMessageInt(dEnter, dtThread, nil, AThreadName, [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogEnterThread(AThread: TThread; AThreadName: WideString; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      LogMessageInt(dEnter, dtThread, AThread, AThreadName, [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogEnterUnitInitialization(AUnitName: WideString);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      LogMessageInt(dEnter, dtUnitConstructor, nil, AUnitName, [], dlvControl);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogEnterMethod(AMethodName: WideString; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dEnter, dtMethod, nil, AMethodName, [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogError(AErrorCode: cardinal; AErrorMessage: string; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dError, dtNone, nil, Format('%s (error code %d: %s)', [AErrorMessage, AErrorCode, SysErrorMessage(AErrorCode)]), [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogError(AErrorMessage: WideString; ALevel: TDebugLevel; AObject: TObject);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dError, dtNone, AObject, AErrorMessage, [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

{$IFDEF Windows}

procedure TDebugLogger.LogException(AException: TExceptionPointers; AMessage: string; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dError, dtNone, nil, AMessage + '[' + IntToHex(AException.ExceptionRecord^.ExceptionCode, 8) + ' @ 0x' +
         IntToHex(cardinal(AException.ExceptionRecord^.ExceptionAddress), 8) + ']', [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

{$ENDIF Windows}

procedure TDebugLogger.LogLeaveMethod(AObject: TObject; AMethodName: WideString; const AArgs: array of const; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dLeave, dtMethod, AObject, AMethodName, AArgs, ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogLeaveMethod(AObject: TObject; AMethodName: WideString; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dLeave, dtMethod, AObject, AMethodName, [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogLeaveMethod(AMethodName: WideString; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dLeave, dtMethod, nil, AMethodName, [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogLeaveMethod(AMethodName: WideString; const AArgs: array of const; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dLeave, dtMethod, nil, AMethodName, AArgs, ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogLeaveProcess(AProcessName: ansistring);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      LogMessageInt(dLeave, dtProcess, nil, AProcessName, [], dlvControl);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogLeaveProcess(AProcessName: WideString);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      LogMessageInt(dLeave, dtProcess, nil, AProcessName, [], dlvControl);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogLeaveThread(AThreadName: WideString; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      LogMessageInt(dLeave, dtThread, nil, AThreadName, [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogLeaveThread(AThread: TThread; AThreadName: WideString; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      LogMessageInt(dLeave, dtThread, AThread, AThreadName, [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogLeaveUnitFinalization(AUnitName: WideString);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      LogMessageInt(dLeave, dtUnitDestructor, nil, AUnitName, [], dlvControl);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogLeaveUnitInitialization(AUnitName: WideString);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      LogMessageInt(dLeave, dtUnitConstructor, nil, AUnitName, [], dlvControl);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogMessage(AMessage: ansistring; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dMessage, dtNone, nil, WideString(AMessage), [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogMessage(AMessage: WideString; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dMessage, dtNone, nil, AMessage, [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogMessage(AObject: TObject; AMessage: ansistring; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dMessage, dtNone, AObject, WideString(AMessage), [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogMessage(AObject: TObject; AMessage: WideString; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dMessage, dtNone, AObject, AMessage, [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogMessageInt(ADirection: TDebugDirection; AType: TDebugType; AObject: TObject; AText: WideString; const AArgs: array of const; ALevel: TDebugLevel);
var
   sDirection, sType, sObjectPrefix, sLine: WideString;

   function FormatArg(AArg: TVarRec): WideString;
   begin
      case AArg.VType of
         vtWideString:
            Result := '''' + WideString(AArg.VWideString) + '''';
         vtPWideChar:
            Result := '''' + WideString(AArg.VPWideChar) + '''';
         vtUnicodeString:
            Result := '''' + WideString(unicodestring(AArg.VUnicodeString)) + '''';
         vtAnsiString:
            Result := '''' + WideString(ansistring(AArg.VAnsiString)) + '''';
         vtPChar:
            Result := '''' + WideString(ansistring(AArg.VPChar)) + '''';
         vtBoolean:
            Result := BoolToStr(AArg.VBoolean, True);
         vtInteger:
            Result := IntToStr(AArg.VInteger);
         else
            Result := '?';
      end;
   end;

   function ArgLine: WideString;
   var
      iArg: integer;
   begin
      Result := '';
      if Length(AArgs) > 0 then begin
         Result := FormatArg(AArgs[low(AArgs)]);
         for iArg := low(AArgs) + 1 to high(AArgs) do begin
            Result := Result + ', ' + FormatArg(AArgs[iArg]);
         end;
      end;
   end;

begin
   sDirection := '?';
   case ADirection of
      dEnter:
         sDirection := '>';
      dLeave:
         sDirection := '<';
      dMessage:
         sDirection := 'i';
      dError:
         sDirection := '!';
      dWarning:
         sDirection := '!';
   end;
   sType := '';
   case AType of
      dtProcess:
         sType := 'process';
      dtThread:
         sType := 'thread';
      dtMethod:
         sType := 'method';
      dtUnitConstructor:
         sType := 'unit initialization';
      dtUnitDestructor:
         sType := 'unit finalization';
      dtVariable:
         sType := 'var';
      else
         sType := '';
   end;
   if Assigned(AObject) then begin
      try
         {$IF DEFINED(VER220)}
         sObjectPrefix := AObject.UnitName + '.' + AObject.ToString + '.'; // AObject.ClassName + '.';
         {$ELSEIF DEFINED(FPC)}
         sObjectPrefix := UTF8Decode(AObject.UnitName) + '.' + UTF8Decode(AObject.ToString) + '.'; // AObject.ClassName + '.';
         {$ELSE}
         sObjectPrefix := AObject.ClassName + '.';
         {$IFEND}
      except
         sObjectPrefix := '(unknown)';
      end;
   end else begin
      sObjectPrefix := '';
   end;
   if Length(sType) > 0 then begin
      sType := '[' + sType + '] ';
   end;
   sDirection := ' ' + sDirection + ' ';
   sLine := DebugLinePrefixInt + sDirection + sType + sObjectPrefix + AText;
   SNLDebugLastPointTimestamp := Now;
   if Length(AArgs) > 0 then begin
      if (ADirection = dEnter) or (Length(AArgs) > 1) then begin
         sLine := sLine + '(' + ArgLine + ')';
      end else if (ADirection = dLeave) and (Length(AArgs) = 1) then begin
         sLine := sLine + ' = ' + WideFormat('%s', AArgs[low(AArgs)]);
      end;
   end;
   SendDebugLogLineToHandlers(sLine);
   SendDebugLogDataToHandlers(ADirection, AType, AObject, AText, AArgs);
end;

procedure TDebugLogger.LogMessageInt(ADirection: TDebugDirection; AType: TDebugType; AObject: TObject; AText: ansistring; const AArgs: array of const; ALevel: TDebugLevel);
begin
   LogMessageInt(ADirection, AType, AObject, UTF8Decode(AText), AArgs, ALevel);
end;

procedure TDebugLogger.LogVarString(AName: ansistring; AVar: ansistring; ALevel: TDebugLevel);
begin
   LogVariableInt(AName, AVar, ALevel);
end;

procedure TDebugLogger.LogVarBoolean(AName: string; AVar: boolean; ALevel: TDebugLevel);
begin
   LogVariableInt(AName, BoolToStr(AVar, True), ALevel);
end;

procedure TDebugLogger.LogVarCardinal(AName: string; AVar: cardinal; ALevel: TDebugLevel);
begin
   LogVariableInt(AName, Format('%0:d (%0:.8x)', [AVar]), ALevel);
end;

procedure TDebugLogger.LogVariableInt(AVariableName, AVariableData: WideString; ALevel: TDebugLevel);
var
   sText, sLine: WideString;
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   // if ALevel = dlvDefault then begin
   // ALevel := SNLDefaultDebugLevel;
   // end;
   sText := 'var ' + AVariableName + ' = ' + AVariableData + ';';
   sLine := DebugLinePrefixInt + ' ' + sText;
   SNLDebugLastPointTimestamp := Now;
   SendDebugLogLineToHandlers(sLine);
   SendDebugLogDataToHandlers(dMessage, dtVariable, nil, WideFormat('%s = %s', [AVariableName, AVariableData]), [AVariableData]);
   {$IFDEF DebugWindow}
   if SNLDebugUseWindow then begin
      // DebugTreeLog(dMessage, dtVariable, nil, AVariableName + ' = ' + AVariableData);
   end;
   {$ENDIF DebugWindow}
end;

procedure TDebugLogger.LogVariableInt(AVariableName, AVariableData: ansistring; ALevel: TDebugLevel);
begin
   LogVariableInt(UTF8Decode(AVariableName), UTF8Decode(AVariableData), ALevel);
end;

class constructor TDebugLogger.Create;
begin
   FInstance := nil;
end;

class destructor TDebugLogger.Destroy;
begin
   FInstance.Free;
end;

class function TDebugLogger.Instance: TDebugLogger;
begin
   if not Assigned(FInstance) then begin
      FInstance := TDebugLogger.Create;
   end;
   Result := FInstance;
end;

procedure TDebugLogger.LogVarInt64(AName: string; AVar: int64; ALevel: TDebugLevel);
begin
   LogVariableInt(AName, Format('%0:d (%0:.16x)', [AVar]), ALevel);
end;

procedure TDebugLogger.LogVarInteger(AName: string; AVar: integer; ALevel: TDebugLevel);
begin
   LogVariableInt(AName, Format('%0:d (%0:.8x)', [AVar]), ALevel);
end;

procedure TDebugLogger.LogVarString(AName: WideString; AVar: WideString; ALevel: TDebugLevel);
begin
   LogVariableInt(AName, AVar, ALevel);
end;

procedure TDebugLogger.LogVarStringList(AName: string; AVar: TStrings; ALevel: TDebugLevel);
var
   i: integer;
begin
   for i := 0 to Pred(AVar.Count) do begin
      LogVariableInt(AName + '[' + IntToStr(i) + ']', AVar[i], ALevel);
   end;
end;

procedure TDebugLogger.LogVarUInt64(AName: string; AVar: uint64; ALevel: TDebugLevel);
begin
   LogVariableInt(AName, Format('%0:d (%0:.16x)', [AVar]), ALevel);
end;

procedure TDebugLogger.LogWarning(AWarnText: string);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      (*
        if not TestLevel(ALevel) then begin
        Exit;
        end;
      *)
      LogMessageInt(dWarning, dtNone, nil, AWarnText, [], dlvWarning);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogError(AErrorMessage: ansistring; ALevel: TDebugLevel; AObject: TObject);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dError, dtNone, AObject, AErrorMessage, [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogException(AException: Exception; AMessage: string; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dError, dtNone, nil, AMessage + ': ' + AException.Message, [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.LogException(AException: Exception; AObject: TObject; AMessage: string; ALevel: TDebugLevel);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      if not TestLevel(ALevel) then begin
         Exit;
      end;
      LogMessageInt(dError, dtNone, AObject, AMessage + ': ' + AException.Message, [], ALevel);
   finally
      Self.EndWrite;
   end;
end;

function TDebugLogger.GetDebugLevelInDebugMode: TDebugLevel;
begin
   if not Assigned(Self) then begin
      Result := dlvError;
      Exit;
   end;
   Self.BeginRead;
   try
      Result := FDebugLevelInDebugMode;
   finally
      Self.EndRead;
   end;
end;

function TDebugLogger.GetDebugLevelInReleaseMode: TDebugLevel;
begin
   if not Assigned(Self) then begin
      Result := dlvError;
      Exit;
   end;
   Self.BeginRead;
   try
      Result := FDebugLevelInReleaseMode;
   finally
      Self.EndRead;
   end;
end;

function TDebugLogger.GetLogFilename: WideString;
begin
   if not Assigned(Self) then begin
      Result := '';
      Exit;
   end;
   Self.BeginRead;
   try
      Result := FLogFilename;
   finally
      Self.EndRead;
   end;
end;

function TDebugLogger.GetUseConsole: boolean;
begin
   if not Assigned(Self) then begin
      Result := False;
      Exit;
   end;
   Self.BeginRead;
   try
      Result := FUseConsole;
   finally
      Self.EndRead;
   end;
end;

function TDebugLogger.GetUseDebugConsole: boolean;
begin
   if not Assigned(Self) then begin
      Result := False;
      Exit;
   end;
   Self.BeginRead;
   try
      Result := FUseDebugConsole;
   finally
      Self.EndRead;
   end;
end;

function TDebugLogger.GetUseEventLog: boolean;
begin
   if not Assigned(Self) then begin
      Result := False;
      Exit;
   end;
   Self.BeginRead;
   try
      Result := FUseEventLog;
   finally
      Self.EndRead;
   end;
end;

function TDebugLogger.GetUseLogFile: boolean;
begin
   if not Assigned(Self) then begin
      Result := False;
      Exit;
   end;
   Self.BeginRead;
   try
      Result := FUseLogFile;
   finally
      Self.EndRead;
   end;
end;

procedure TDebugLogger.LogEnterUnitFinalization(AUnitName: WideString);
begin
   if not Assigned(Self) then begin
      Exit;
   end;
   Self.BeginWrite;
   try
      LogMessageInt(dEnter, dtUnitDestructor, nil, AUnitName, [], dlvControl);
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.SetDebugLevelInDebugMode(const Value: TDebugLevel);
begin
   Self.BeginWrite;
   try
      FDebugLevelInDebugMode := Value;
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.SetDebugLevelInReleaseMode(const Value: TDebugLevel);
begin
   Self.BeginWrite;
   try
      FDebugLevelInReleaseMode := Value;
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.SetLogFilename(const Value: WideString);
begin
   Self.BeginWrite;
   try
      FLogFilename := Value;
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.SetUseConsole(const Value: boolean);
begin
   Self.BeginWrite;
   try
      if Value then begin
         Include(DebugOutputs, doConsole);
      end else begin
         Exclude(DebugOutputs, doConsole);
      end;
      FUseConsole := Value;
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.SetUseDebugConsole(const Value: boolean);
begin
   Self.BeginWrite;
   try
      if Value then begin
         Include(DebugOutputs, doDebugConsole);
      end else begin
         Exclude(DebugOutputs, doDebugConsole);
      end;
      FUseDebugConsole := Value;
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.SetUseEventLog(const Value: boolean);
begin
   Self.BeginWrite;
   try
      FUseEventLog := Value;
      if Value then begin
         Include(DebugOutputs, doEventLog);
      end else begin
         Exclude(DebugOutputs, doEventLog);
      end;
   finally
      Self.EndWrite;
   end;
end;

procedure TDebugLogger.SetUseLogFile(const Value: boolean);
begin
   Self.BeginWrite;
   try
      FUseLogFile := Value;
      if Value then begin
         Include(DebugOutputs, doLogFile);
      end else begin
         Exclude(DebugOutputs, doLogFile);
      end;
   finally
      Self.EndWrite;
   end;
end;

function TDebugLogger.TestLevel(ALevel: TDebugLevel): boolean;
begin
   {$IFDEF Debug}
   Result := ALevel >= FDebugLevelInDebugMode;
   {$ELSE Debug}
   Result := ALevel >= FDebugLevelInReleaseMode;
   {$ENDIF Debug}
end;

function TDebugLogger.TestUseConsole: boolean;
begin
   Result := FUseConsole;
   {$IFDEF Windows}
   if FUseConsole and (not IsConsole) then begin
      IsConsole := AllocConsole;
   end;
   {$ENDIF Windows}
end;

initialization

   snlDebugInitialization;
   {$IFDEF Unicode}
   DeleteFileW(pwidechar(ParamStr(0) + '.log'));
   {$ELSE}
   SysUtils.DeleteFile(PChar(ParamStr(0) + '.log'));
   {$ENDIF}
   DebugLogger.UseLogFile := True;

finalization

   snlDebugFinalization;

end.
