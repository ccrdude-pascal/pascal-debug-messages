﻿{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Test case for PepiMK.Debug.Destinations.LogFile.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2016-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-03-21  pk   5m  Added test case checking log writing and log content
// 2016-05-11  pk  10m  Wrote test case
// *****************************************************************************
   )
}

unit DebugLog.Destinations.LogFile.TestCase;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ELSE FPC}
{$DEFINE Windows}
{$ENDIF FPC}

interface

uses
   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   TestFramework,
   {$ENDIF FPC}
   Classes,
   SysUtils,
   Firefly.Tests,
   DebugLog,
   DebugLog.Consts,
   DebugLog.Destinations.LogFile;

type

   { TTestCase_DebugLog_Destinations_LogFile }

   TTestCase_DebugLog_Destinations_LogFile = class(TPKTestCase)
   published
      procedure TestHandlerExists;
      procedure TestLogFile;
   end;

implementation

const
   STestLine = 'test National Geographic Calendar';

{ TTestCaseLicensesCredits }

procedure TTestCase_DebugLog_Destinations_LogFile.TestHandlerExists;
begin
   CheckTrue(IsRegisteredDebugLogLineHandler(@SendDebugLogLineToLogFile));
end;

procedure TTestCase_DebugLog_Destinations_LogFile.TestLogFile;
var
   sl: TStringList;
begin
   DeleteFile(GetDebugLogFilename);
   DebugLogger.LogMessage(STestLine);
   CheckTrue(FileExists(GetDebugLogFilename));
   sl := TStringList.Create;
   try
      sl.LoadFromFile(GetDebugLogFilename);
      CheckTrue(Pos(STestLine, sl.Text)>0, 'test line in file');
   finally
      sl.Free;
   end;
end;

initialization

   RegisterTest('DebugLog', TTestCase_DebugLog_Destinations_LogFile.Suite);

end.
