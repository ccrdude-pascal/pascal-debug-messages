program FireflyDebugLogUnitTests;

{$mode objfpc}{$H+}
{$apptype console}

uses
   Interfaces, Forms, DebugLog.Consts.TestCase,
   DebugLog.Destinations.DebugConsole.TestCase,
   DebugLog.Destinations.LogFile.TestCase,
   DebugLog.Destinations.Console.TestCase, DebugLog.CodePerformance.TestCase,
   DebugLog.TestCase, DebugLog.CodePerformance, DebugLog.Consts,
   DebugLog.Destinations.Console, DebugLog.Destinations.DebugConsole,
   DebugLog.Destinations.LogFile, DebugLog, GuiTestRunner;

{$R *.res}

begin
   Application.Initialize;
   Application.CreateForm(TGuiTestRunner, TestRunner);
   Application.Run;
end.
