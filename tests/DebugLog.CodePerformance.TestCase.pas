{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Test case for PepiMK.Debug.CodePerformance.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-03-06  pk  ---  Test case created by PascalHeaderUpdate
// *****************************************************************************
   )
}

unit DebugLog.CodePerformance.TestCase;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   TestFramework,
   {$ENDIF FPC}
   Firefly.Tests,
   DebugLog.CodePerformance;

type

   {
     TTestCase_DebugLog_CodePerformance is a test class for classes, functions and procedures
     from unit PepiMK.Debug.CodePerformance.
   }
   TTestCase_DebugLog_CodePerformance = class(TPKTestCase)
   protected
      procedure SetUp; override;
   published
      {
        TestCompile is a dummy that simply fills in code to check if
        this test unit, and the associated tested unit, get compiled.
      }
      procedure TestCompile;
      procedure TestCount;
   end;

implementation

uses
   SysUtils,
   Windows;

{ TTestCase_DebugLog_CodePerformance }

procedure TTestCase_DebugLog_CodePerformance.SetUp;
begin
   inherited SetUp;
   InitRunTimeManager;
end;

procedure TTestCase_DebugLog_CodePerformance.TestCompile;
begin
   CheckTrue(true);
end;

procedure TTestCase_DebugLog_CodePerformance.TestCount;
var iBefore, iAfter: integer;
  s: string;
begin
   iBefore := RunTimeManager.MeasureList.Count;
   RunTimeManager.AddPoint('test 1');
   RunTimeManager.AddPoint('test 2');
   RunTimeManager.AddPoint('test 3');
   iAfter := RunTimeManager.MeasureList.Count;
   CheckEquals(3, iAfter-iBefore, 'RunTimeManager.MeasureList.Count diff');
   s := RunTimeManager.MeasureList[Pred(iAfter)].Text;
   CheckTrue(Pos('test 3', s)>0, '"test 3" in last point');
end;

initialization

   RegisterTest('DebugLog', TTestCase_DebugLog_CodePerformance);

end.
